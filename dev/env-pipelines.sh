# bash

# --------------------
# Source environment
# --------------------
pypette=$(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]})))
source "${pypette}/bin/source.sh"
source pypette.sh

# ---------------
# Conda Env Mode
# ---------------
function checkEnvMode() {
  export ENV_MODE="${1:-}"
  pypette::checkEnvMode
  # Ensure prod env name has no suffix.
  [ "$ENV_MODE" != 'production' ] || ENV_MODE=''
}

# -------------------
# Conda Env Commands
# -------------------
function mambaOrCondaExec() {
  #
  # Tries using mamba if it exists, conda otherwise.
  #

  type mamba &> /dev/null \
  && mamba $@             \
  || conda $@
}

function exportCondaEnv() {
   mambaOrCondaExec env export -n $1
}

function createCondaEnv() {
  mambaOrCondaExec env create --file "$1"
}

# -------------
# Conda Files
# -------------
function pipelineRequFile() {
  printf "$(pypette::pipelineDir ${1})/conda.yaml"
}

function pipelineRequFileTmp() {
  printf "$(pypette::pipelineDir ${1})/conda.installed.yaml"
}

function pipelineEnvName() {
  printf "pypette-${1}${ENV_MODE:+-$ENV_MODE}"
}

# ----------------
# Conda Commands
# ----------------
function exportPipelineEnv() {
  local envName=$(pipelineEnvName $1)
  printf "> Exporting pipeline env '$envName'..." >&2
  exportCondaEnv "$envName" \
    && printf " OK\n" >&2   \
    || printf " KO\n" >&2
}

function envPipelinesApplyFunc() {
  #
  # Applies the given function to each given pipeline.
  # $1: function name
  # $2: pipelines. Default all pipelines.
  # $@: extra parameters.
  #
  local func pipelines params
  func="$1" && shift
  pipelines="${1:-$(pypette::pipelines | xargs)}"
  [ $# -eq 0 ] || shift
  echo "Pipelines: [$pipelines]" >&2
  params=${@:-}
  echo "params: [${@:-}]" >&2
  for pipeline in ${pipelines}; do
    $func $pipeline $params
  done
}

function createPipelinesEnv() {
  echo envPipelinesApplyFunc createPipelineEnv "${@}" >&2
  envPipelinesApplyFunc createPipelineEnv "$@"
}

function createPipelineEnv() {
  local pipeline envName
  pipeline="$1"
  envName=$(pipelineEnvName $pipeline)
  printf "> Creating pipeline env '$envName'...\n" >&2
  createPipelineRequFileTmp "$pipeline"
  createCondaEnv $(pipelineRequFileTmp $pipeline) \
    && printf " OK\n" >&2                         \
    || printf " KO\n" >&2
}

function createPipelineRequFileTmp() {
  local pipeline envName pipelineRequFile pipelineRequFileTmp
  pipeline="$1" 
  envName=$(pipelineEnvName $pipeline)
  pipelineRequFile=$(pipelineRequFile $pipeline)
  pipelineRequFileTmp=$(pipelineRequFileTmp $pipeline)
  cat $pipelineRequFile | condaDepsPatchless | updateEnvName $pipeline > $pipelineRequFileTmp
  pipDeps            < $pipelineRequFile >> $pipelineRequFileTmp
}

function rmPipelinesEnv() {
  envPipelinesApplyFunc rmPipelineEnv $@
}

function rmPipelineEnv() {
  local pipeline envName
  pipeline="$1" 
  envName=$(pipelineEnvName $pipeline)
  mambaOrCondaExec env remove -n $envName
}

function updatePipelineEnv() {
  local pipeline="$1"
  rmPipelineEnv "$pipeline"
  createPipelineEnv "$pipeline"
}

function installPipelinesPkgs() {
  envPipelinesApplyFunc installPipelinePkgs "$@"
}

function installPipelinePkgs() {
  local pipeline pkgs envName
  pipeline="$1" && shift
  pkgs="$@"
  envName=$(pipelineEnvName $pipeline)
  mambaOrCondaExec install -y -n $envName $pkgs
}

# --------------------
# Conda Requirements
# --------------------
function updateEnvName() {
  cat /dev/stdin | sed '1 s/^.*$/name: '$(pipelineEnvName $1)'/'
}

function condaDepsPatchless() {
  #
  # Removes patch version from conda dependencies.
  # Reads from STDIN.
  #
  cat /dev/stdin | condaDeps | rmCondaPatches
}

function filterDepsLines() {
  #
  # Filters conda deps lines.
  # Reads from STDIN.
  #
  cat /dev/stdin | grep -v '^prefix' | sed '/^$/d'
}

function condaDeps() {
  #
  # Returns conda dependencies.
  # Reads from STDIN.
  #
  local data pipDepsLines pipDepsStartingLine
  data=$(cat /dev/stdin | filterDepsLines)
  pipDepsNbLines=$(pipDepsNbLines <<< "$data")
  head -n -$pipDepsNbLines <<< "$data"
}

function pipDeps() {
  #
  # Returns pip dependencies.
  # Reads from STDIN.
  #
  local data pipDepsNbLines
  data=$(cat /dev/stdin | filterDepsLines)
  pipDepsNbLines=$(pipDepsNbLines <<< "$data")
  if [ $pipDepsNbLines -gt 0 ]; then
    tail -n ${pipDepsNbLines} <<< "$data"
  else
    :
  fi
}

function pipDepsNbLines() {
  #
  # Returns number of lines in pip dependencies.
  # Reads from STDIN.
  #
  local data pipStart pipLines
  data=$(cat /dev/stdin)
  pipStart=$(pipDepsLineStart <<< $data)
  if [ -z ${pipStart:+x} ]; then
    pipLines=0
  else
    pipLines=$(
      tail -n +${pipStart} <<< "$data" \
      | grep -n '^    - '              \
      | wc -l  )
  fi
  printf $((pipLines+1))
}

function pipDepsLineStart() {
  #
  # Returns line where pip dependencies start.
  # Reads from STDIN.
  #
  cat /dev/stdin | grep -n '^  - pip:' | cut -d: -f1
}

function pipDepsGrepPattern() {
  cat /dev/stdin | grep -n '^    - '
}

function rmCondaPatches() {
  #
  # Removes patch versions from conda dependencies.
  # Reads from STDIN.
  #
  cat /dev/stdin | cut -d= -f1,2
}

# ------------
# Parameters
# ------------
function optParsePipelines() {
  optParseShortLongOpts p pipeline <<< $@
}

function optParseShortLongOpts() {
  #
  # Parses a given string of parameters according to the given :short: and :long: options.
  # :short: and :long: option string to look up given as parameters.
  # Parameter strings given to STDIN.
  #
  local params=() short="${1:+-$1}" long="${2:+--$2}"
  set -- $(cat /dev/stdin)
  while [ $# -ge 1 ]; do
    case "$1" in
      $short|$long)
        params+=($2) && shift
        ;;
    esac
    shift
  done
  echo "${params[@]}"
}

function optParseFreeParams() {
  #
  # Parses the given free parameters.
  # Options are ignored.
  #
  local params=() 
  while [ $# -ge 1 ]; do
    case "$1" in
      -*) shift ;;
      *) params+=($1);;
    esac
    shift
  done
  echo "${params[@]}"
}

function checkHelp() {
  grep -qs -- '-h\|--help' <<< $@ && dev::manual && exit || :
}
