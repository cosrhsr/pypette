def vc_fb__filtering_string():
    return "( QUAL > 0 ) "           \
       + "& ( QUAL / AO > 1 ) "      \
       + "& ( SAF > 0 & SAR > 0 ) "  \
       + "& ( RPR > 1 & RPL > 1 ) "  \
       + "& ( MQM > 10 | MQMR > 10 )"
