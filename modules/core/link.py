def symlinkCmd(target, linkname, deref=True):
  """
  Returns the command to symlink the :output: to the :input:.
  Symlinks are dereferenced by default.
  """
  cmd = ""
  if deref:
    cmd = f"""
      ln -sfn $(readlink -f {target}) {linkname}
    """ 
  else:
    cmd = f"""
      ln -sfn {target} {linkname}
    """
  return cmd

def hardlinkCmd(target, linkname):
  """
  Returns the command to hardlink :output: to the :input:.
  Note: Actually duplicates the data as all filesystems do not allow
  hardlinking across different folders (Ex. BEEGFS).
  """
  return f"rsync -l {target} {linkname}"
