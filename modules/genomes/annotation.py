def annot__dir(**kwargs):
  return os.path.join(
    genome__dir(**kwargs),
    "annotations")

@genome__formatSpeciesCfg
def annot__gencodeDir(**kwargs):
  return os.path.join(
    annot__dir(**kwargs),
    "{species.genome.assembly.gencodeRelease}")

def annot__getGenomeName():
    gnmName = project__speciesGenomeName()
    return 'hg19' if gnmName == 'hs37d5' else gnmName

def annot__getEnsembleBuildVersion():
    if project__speciesGenomeName() == "hs37d5":
        return "GRCh37.75"
    else:
        return project__ensemblBuildVersion()

# ----------------------
# EBI Annotation Files
# ----------------------
@genome__formatSpeciesCfg
def annot__ebiBaseUrl():
  return pypette.config.databases.ebi.gencodeAnnotBaseName

@genome__formatSpeciesCfg
def annot__ebiGtfUrl():
  return os.path.join(
    genome__ebiReleaseUrl(),
    annot__ebiBaseUrl() + ".gtf.gz")

def annot__ebiBase(**kwargs):
  return os.path.join(
    annot__gencodeDir(**kwargs),
    "ebi",
    annot__ebiBaseUrl())

def annot__ebiGtf(**kwargs):
  return annot__ebiBase(**kwargs) + ".gtf"

def annot__ebiGtfGz(**kwargs):
  return annot__ebiGtf(**kwargs) + ".gz"

def annot__ebiBiotypes():
  return annot__ebiBase() + ".biotypes.tsv.gz"

def annot__ebiBed():
  return annot__ebiBase() + ".bed"

# -----------------------
# UCSC Annotation Files
# -----------------------
@genome__formatSpeciesCfg
def annot__ucscBaseUrl():
  return pypette.config.databases.ucsc.gencodeAnnotBaseName

@genome__formatSpeciesCfg
def annot__ucscTxtUrl():
  return os.path.join(
    pypette.config.databases.ucsc.genomeUrl,
    annot__ucscBaseUrl() + ".txt.gz")

def annot__ucscBase(**kwargs):
  return os.path.join(
    annot__gencodeDir(**kwargs),
    "ucsc",
    annot__ucscBaseUrl())

def annot__ucscTxt(**kwargs):
  return annot__ucscBase(**kwargs) + ".txt.gz"

def annot__ucscGenePred(**kwargs):
  return annot__ucscBase(**kwargs) + ".genePred.gz"

def annot__ucscGtf(**kwargs):
  return annot__ucscBase(**kwargs) + ".gtf"

def annot__ucscGtfGz(**kwargs):
  return annot__ucscGtf(**kwargs) + ".gz"

def annot__ucscBedgz(**kwargs):
  return annot__ucscBase(**kwargs) + ".bed.gz"

def annot__ucscBed():
  return annot__ucscBase() + ".bed"

# -------
# COSMIC
# -------
def annot__cosmicDir(**kwargs):
  return os.path.join(
           genome__baseDir(**kwargs), 'hsapiens', 'annotations',
           'cosmic/v94/VCF')

def annot__cosmicGetFile(file, **kwargs):
    return os.path.join(annot__cosmicDir(**kwargs), file)

def annot__cosmicGetUrl(file):
    return os.path.join(config.databases.cosmic.vcfUrl, file)

# -------
# dbNSFP
# -------
def annot__dbNsfpDir(**kwargs):
  return os.path.join(genome__baseDir(**kwargs), 'hsapiens', 'annotations', 'dbNsfp')

def annot__dbNsfpVersionFileFmt(**kwargs):
    return os.path.join(annot__dbNsfpDir(**kwargs),
                        "dbNSFP{version}/dbNSFP{version}.txt.gz")

def annot__dbNsfpVersionFile(**kwargs):
    return annot__dbNsfpVersionFileFmt(**kwargs).format(version=config.databases.dbNsfp.version)

def annot__dbNsfpGetFile(file, **kwargs):
    return os.path.join(annot__dbNsfpDir(**kwargs), file)

def annot__dbNsfpGetUrl(file):
    return os.path.join(config.databases.dbNsfp.url, file)

# -------
# dbSNP
# -------
def annot__dbSnpDir(**kwargs):
  return os.path.join(
           annot__dir(**kwargs),
           "dbSNP")

def annot__dbSnpGetFile(file, **kwargs):
    return os.path.join(annot__dbSnpDir(**kwargs), file)

# ----------------
# Broad Institute
# -----------------
def annot__broadinstituteDir(**kwargs):
  return os.path.join(
           annot__dir(**kwargs),
           "broadinstitute")

def annot__broadinstituteBundleDir(**kwargs):
  return os.path.join(annot__broadinstituteDir(**kwargs),
                      "bundle")

def annot__broadinstituteKWFmt():
  gnmName = annot__getGenomeName()
  return { 
    'gnmName': gnmName,
    'siteExt': '.sites' if gnmName == 'hg19' else ''
  }

def annot__broadinstituteFilePath(file, **kwargs):
  return os.path.join(annot__broadinstituteBundleDir(**kwargs), file)

def annot__broadinstituteHapmap(**kwargs):
  return annot__broadinstituteFilePath("hapmap_3.3.{gnmName}{siteExt}.vcf.gz"
                                         .format(**annot__broadinstituteKWFmt()),
                                       **kwargs)

def annot__broadinstituteOmni(**kwargs):
  return annot__broadinstituteFilePath("1000G_omni2.5.{gnmName}{siteExt}.vcf.gz"
                                         .format(**annot__broadinstituteKWFmt()),
                                       **kwargs)

def annot__broadinstitute1000G(**kwargs):
  return annot__broadinstituteFilePath("1000G_phase1.snps.high_confidence.{gnmName}{siteExt}.vcf.gz"
                                         .format(**annot__broadinstituteKWFmt()),
                                       **kwargs)

def annot__broadinstituteDbSnp(**kwargs):
  return annot__broadinstituteFilePath("dbsnp_138.{gnmName}.vcf.gz"
                                         .format(**annot__broadinstituteKWFmt()),
                                       **kwargs)

def annot__broadinstituteMills(**kwargs):
  return annot__broadinstituteFilePath("Mills_and_1000G_gold_standard.indels.{gnmName}{siteExt}.vcf.gz"
                                         .format(**annot__broadinstituteKWFmt()),
                                       **kwargs)

# --------
# SnpEff
# --------
def annot__snpEffDir(**kwargs):
  return os.path.join(annot__dir(useTaxo=True, **kwargs),
                      "snpEff")

def annot__snpEffDataDir(**kwargs):
  return os.path.join(annot__snpEffDir(**kwargs),
                      "data")

# -------------------
# Base Recalibration
# --------------------
def annot__baseRecalibSites(**kwargs):
  # For now sites available only for human
  if pypette.config.project.species != 'human':
    return []
  gnmName = annot__getGenomeName()

  siteExt = '.sites' if gnmName == 'hg19' else ''

  return [
      f"{annot__broadinstituteBundleDir(**kwargs)}/1000G_phase1.snps.high_confidence.{gnmName}{siteExt}.vcf.gz",
      f"{annot__broadinstituteBundleDir(**kwargs)}/Mills_and_1000G_gold_standard.indels.{gnmName}{siteExt}.vcf.gz",
      f"{annot__broadinstituteBundleDir(**kwargs)}/dbsnp_138.{gnmName}.vcf.gz"
  ]

def annot__baseRecalibSitesIdx(**kwargs):
  return list(map( "{}.tbi".format, annot__baseRecalibSites(**kwargs) ))

# ------
# GATK
# ------
def annot__gatkDir(**kwargs):
  return os.path.join(
           annot__dir(**kwargs),
           "GATK")

def annot__gatkGetDbSnpFile(**kwargs):
    gnmName = annot__getGenomeName()
    dbsnpV  = config.databases.dbSnp.version
    return os.path.join(annot__broadinstituteBundleDir(**kwargs),
                        f"dbsnp_{dbsnpV}.{gnmName}.vcf.gz")

def annot__gnomadFile(**kwargs):
    gnmName = annot__getGenomeName()
    return os.path.join(annot__gatkDir(**kwargs),
                        f"af-only-gnomad.{gnmName}.vcf.gz")

