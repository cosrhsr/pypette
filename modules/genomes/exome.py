EXOME_SPLIT_SCATTER = 100
def exome__targetDir(**kwargs):
  return os.path.join(
    annot__dir(**kwargs),
    "exomes_targets")

def exome__intervalListFmt(**kwargs):
  return os.path.join(exome__targetDir(**kwargs),
    "{kit}_{interval}.interval_list")

def exome__targetIntervals(**kwargs):
  return exome__intervalListFmt(**kwargs).format(
           kit      = config.pipeline.modules.exomeIntervals.kit,
           interval = config.pipeline.modules.exomeIntervals.target)

def exome__baitIntervals(**kwargs):
  return exome__intervalListFmt(**kwargs).format(
           kit      = config.pipeline.modules.exomeIntervals.kit,
           interval = config.pipeline.modules.exomeIntervals.bait)

def exome__splitIntervalListsDir(**kwargs):
  return exome__targetIntervals(**kwargs) + '.splits'

def exome__splitIntervalListFmt(**kwargs):
  return os.path.join(
           exome__splitIntervalListsDir(**kwargs),
           '{intervalNb}-scattered.interval_list' )
