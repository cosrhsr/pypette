import yaml
from collections import OrderedDict

def writeDict(data, output):
  """
  Dumps :data: in the given yaml file :output:
  Manages OrderedDict.
  """
  setupRepesenter()
  with open(output, 'w') as fd:
    yaml.dump(data, fd, Dumper=MyDumper, default_flow_style=False)

class MyDumper(yaml.Dumper):
  def increase_indent(self, flow=False, indentless=False):
    return super(MyDumper, self).increase_indent(flow, False)

def representDictionaryOrder(self, dict_data):
  return self.represent_mapping('tag:yaml.org,2002:map', dict_data.items())

def setupRepesenter():
  yaml.add_representer(OrderedDict, representDictionaryOrder)
