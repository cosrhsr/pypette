CURR_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]} ) )
#source-miniconda3;
#export PYPETTE_CONDA_PATH="${COSR_TOOLS_DIR}/miniconda3"
export PYPETTE_CONDA_PATH="${HOME}/miniconda3"
source ${HOME}/dev/pypette/bin/source.sh
type pypette
tail "${CURR_DIR}/pypette/usage.csv"

cat << 'eol'
spl=PD
run='190610_M00571_0401_000000000-C92M3'
time pypette-dna-wgs --debug  -j 32 --stats --snake-opts "--notemp"  \
  samples/${spl}/runs/${run}/fastq/merge-by-read/trimmed/bbduk/mapped/bwa/${spl}.bam

rm -r genomes/ samples/PD/runs/${run}/fastq/merge-by-read/trimmed/bbduk/mapped
time pypette-dna-wgs --debug  -j 32 --stats --snake-opts "--notemp" \
  samples/${spl}/runs/${run}/fastq/merge-by-read/trimmed/bbduk/mapped/bwa/${spl}.bam; tree genomes

eol
